import torch
from deepfool import deepfool
import numpy as np

import matplotlib.pyplot as plt

from LENET import  train_eval_lenet
from Alexnet import train_eval_alexnet
from NIN import  train_eval_NIN
from Data_load import  load_mnist,load_Cifar10
from accuracy_test import get_accuracy_single_image
from fault_model import fault_model_one_weight
DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
def ComputeRobustness(model,fault_model,vali_data_loder):
        robustness_orig_model = 0
        robustness_fault_model = 0

        total = 0
        correct_predict_orig = 0
        correct_predict_pert = 0
        correct_predict_fault_model = 0
        correct_predict_fault_pert = 0

        for images,lables in vali_data_loder:
            # print(images.size())

            for i in range(images.size()[0]):
                # print(i)
                # print(images[i].size())
                total+=1
                n = get_accuracy_single_image(model,images[i],lables[i],DEVICE)
                correct_predict_orig+=n
                n2 = get_accuracy_single_image(fault_model, images[i], lables[i], DEVICE)
                correct_predict_fault_model+=n2
                r_tot, loop_i, label, k_i, pert_image = deepfool(images[i],model)
                r_tot_fault, loop_i_fault, label_fault, k_i_fault, pert_image_fault = deepfool(images[i], fault_model)
                # print(pert_image.size())
                n1 = get_accuracy_single_image(model,pert_image,lables[i],DEVICE)
                n3 = get_accuracy_single_image(fault_model, pert_image, lables[i], DEVICE)
                correct_predict_pert+=n1
                correct_predict_fault_pert+=n3


                # print(r_tot.shape)
                robustness_orig_model += (np.linalg.norm(r_tot.flatten(),2)/np.linalg.norm(images[i].numpy().flatten(),2))
                robustness_fault_model += (
                            np.linalg.norm(r_tot_fault.flatten(), 2) / np.linalg.norm(images[i].numpy().flatten(), 2))
        robustness_orig_model = robustness_orig_model / total
        robustness_fault_model = robustness_fault_model/total
        accuracy_orginal = correct_predict_orig/total
        accuracy_pert = correct_predict_pert/total
        accuracy_fault = correct_predict_fault_model/total
        accuracy_fault_pert = correct_predict_fault_pert/total
        print("robustness_orginal:{}".format(robustness_orig_model))
        print("robustness_fault:{}".format(robustness_fault_model))
        print("accuracy_orginal:{}".format(accuracy_orginal))
        print("accuracy_pert:{}".format(accuracy_pert))
        print("accuracy_fault:{}".format(accuracy_fault))
        print("accuracy_fault_pert:{}".format((accuracy_fault_pert)))
        return  robustness_orig_model

if __name__ == '__main__':
    train_loder,test_loader = load_Cifar10()
    model,acc = train_eval_alexnet(20,train_loder,test_loader,training=True)

    fault_model1 = fault_model_one_weight(model, img_size=32, BATCH_SIZE=32, channel=3,
                                         conv_i=1, k=1, c_i=1, h_i=1, w_i=1, value=0.01388985849916935)
    fault_model2= fault_model_one_weight(model, img_size=32, BATCH_SIZE=32, channel=3,
                                         conv_i=1, k=1, c_i=1, h_i=1, w_i=1, value=-10000)
    robustness1 = ComputeRobustness(model,fault_model1,test_loader)
    robustness2 = ComputeRobustness(model, fault_model2, test_loader)
    train_loder, test_loader = load_mnist()
    model,acc = train_eval_lenet(training=True)
    fault_model1 = fault_model_one_weight(model, img_size=32, BATCH_SIZE=32, channel=1,
                                          conv_i=1, k=1, c_i=1, h_i=1, w_i=1, value=0.01388985849916935)
    fault_model2 = fault_model_one_weight(model, img_size=32, BATCH_SIZE=32, channel=1,
                                          conv_i=1, k=1, c_i=1, h_i=1, w_i=1, value=-10000)
    robustness1 = ComputeRobustness(model,fault_model1,test_loader)
    robustness2 = ComputeRobustness(model, fault_model2, test_loader)

    train_loder, test_loader = load_Cifar10()
    model, acc = train_eval_NIN(model,10,train_loder,test_loader,training=True)

    fault_model1 = fault_model_one_weight(model, img_size=32, BATCH_SIZE=32, channel=3,
                                          conv_i=1, k=1, c_i=1, h_i=1, w_i=1, value=0.01388985849916935)
    fault_model2 = fault_model_one_weight(model, img_size=32, BATCH_SIZE=32, channel=3,
                                          conv_i=1, k=1, c_i=1, h_i=1, w_i=1, value=-10000)
    robustness1 = ComputeRobustness(model, fault_model1, test_loader)
    robustness2 = ComputeRobustness(model, fault_model2, test_loader)

