
import torch
import torchvision.datasets as datasets
from torchvision.transforms import transforms
from torch.utils.data import DataLoader,Dataset
import pandas as pd
import sys
import numpy as np
import os
# def load_imagenet():
#     PATH = "valid"
#     image_name = 'ILSVRC2012_val_' + str(5300).zfill(8) + '.JPEG'
#     folder_name = '197'
#     img = plt.imread(f'{PATH}{folder_name}/{image_name}')
#     plt.imshow(img)




def load_mnist():
    BATCH_SIZE = 32
    # define transforms
    transform = transforms.Compose([transforms.Resize((32, 32)),
                                     transforms.ToTensor()])

    # download and create datasets
    train_dataset = datasets.MNIST(root='./data',
                                   train=True,
                                   transform=transform,
                                   download=True)

    valid_dataset = datasets.MNIST(root='./data',
                                   train=False,
                                   transform=transform)

    # define the data loaders
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=BATCH_SIZE,
                              shuffle=True)

    valid_loader = DataLoader(dataset=valid_dataset,
                              batch_size=BATCH_SIZE,
                              shuffle=False)
    return train_loader,valid_loader
def load_Cifar10():
    BATCH_SIZE = 64
    transform = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
        ]
    )
    trainset = datasets.CIFAR10(
        root="./data", train=True, download=True, transform=transform
    )
    train_loader = torch.utils.data.DataLoader(
        trainset, batch_size=BATCH_SIZE, shuffle=True, num_workers=1
    )

    testset = datasets.CIFAR10(
        root="./data", train=False, download=True, transform=transform
    )

    # TODO use the same image across the whole batch
    val_loader = torch.utils.data.DataLoader(
        testset, batch_size=BATCH_SIZE, shuffle=False, num_workers=1
    )
    return  train_loader,val_loader