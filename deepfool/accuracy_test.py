import torch
from torch.autograd import Variable
def get_accuracy(model, data_loader, device):
    '''
    Function for computing the accuracy of the predictions over the entire data_loader
    '''

    correct_pred = 0
    n = 0

    with torch.no_grad():
        model.eval()
        for X, y_true in data_loader:
            X = X.to(device)
            y_true = y_true.to(device)

            _, y_prob = model(X)
            _, predicted_labels = torch.max(y_prob, 1)

            n += y_true.size(0)
            correct_pred += (predicted_labels == y_true).sum()

    return correct_pred.float() / n
def get_accuracy_single_image(model, image,label, device):
    correct_pred = 0
    with torch.no_grad():
        model.eval()
        image = image.to(device)
        label = label.to(device)
        if len(image.size())!=4:
            _, y_prob = model.forward(Variable(image[None, :, :, :]))
        else:
            _, y_prob = model.forward(Variable(image))


        _, predicted_labels = torch.max(y_prob, 1)
        if predicted_labels == label:
            correct_pred+=1
    return  correct_pred
